/**
* @author Team Billion
*/

var net = require("net");
var JSONStream = require('JSONStream');

var Billion = require("./lib/billion.js");

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

//MULTI CAR TESTING
var MULTI_CAR_ENABLED = false;
var SINGLE_INSTANCE = false;

var MULTI_CAR_TRACK = "keimola";
var MULTI_CAR_PASSWORD = "joohs";
var MULTI_CAR_COUNT = 2;

if (MULTI_CAR_ENABLED) {
  //Create random bot name
  for (var i = 0; i < (SINGLE_INSTANCE ? MULTI_CAR_COUNT : 1); i++) {
    setTimeout(initTimeout(i), i * 100);
  }
} else {
  initBillion(botName, 0);
}

function initTimeout(index) {
  return function() {
    initBillion(Math.random().toString(36).substring(0,5), index);
  };
}

function initBillion(name, index) {
  var billion = new Billion(name, index);
  console.log("I'm", name, "and connect to", serverHost + ":" + serverPort);
  
  var client = net.connect(serverPort, serverHost, function() {
    client.setNoDelay(true);
    
    if (MULTI_CAR_ENABLED) {
      console.log("Joining race, waiting for total of "+MULTI_CAR_COUNT+" bots...");
      return send({
        msgType: "joinRace",
        data: {
          botId: {
            name: name,
            key: botKey
          },
          trackName: MULTI_CAR_TRACK,
          password: MULTI_CAR_PASSWORD,
          carCount: MULTI_CAR_COUNT
        }
      });
    } else {
      return send({
        msgType: "join",
        data: {
          name: botName,
          key: botKey
        }
      });
    }
  });
  
  var jsonStream = client.pipe(JSONStream.parse());

  jsonStream.on('data', function(data) {
    var response = billion.getResponseMessage(data);
    if (response && response.msgType !== undefined &&
        response.data !== undefined) {
      //console.log("RESPONSE: msgType:"+response.msgType+" data:"+response.data);
      send(response);
    } else {
      send({
        msgType: "ping",
        data: {}
      });
    }
  });
  
  jsonStream.on('error', function() {
    return console.log("disconnected");
  });
  
  jsonStream.on('end', function() {
    return console.log("disconnected ended");
  });
  
  jsonStream.on('timeout', function() {
    return console.log("timeouted");
  });
  
  jsonStream.on('close', function(had_error) {
    return console.log("closed " + had_error);
  });
    
  function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
  }
}
