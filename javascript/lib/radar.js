/**
* @author Team Billion
*/

//require("./track.js");

var Radar = function(carID) {
  this.carID = carID;
  this.track = null;
  this.myCar = null;
  this.otherCars = [];

  //Constants
  this.laneWarningDistance = 100.0;
  this.collisionWarningDistance = 30.0;
};

/**
* Add car objects you want to track with radar.
* Should include all cars in race.
*/
Radar.prototype.trackCars = function(cars, track) {
  //Set track
  this.track = track;

  //Find my car, other cars
  for (carName in cars) {
    var car = cars[carName];
    if (car.id.name == this.carID.name &&
	car.id.color == this.carID.color) {
      this.myCar = car;
    } else {
      this.otherCars.push(car);
    }
  }
};

/**
* Call this on every game tick to update radar calculations.
* Returns warning messages.
*/
Radar.prototype.warnings = function() {
  var warning = null;
  //TODO: Calculations for each game tick, MOAR calculations?
  if (warning == null) {
    warning = this.checkDistances(); 
  }
  /*
  if (warning == null) {
    warning = this.checkLanes();
  }
  */
  return warning;
};

/**
* Checks distances between your car and others. Sends warning callbacks
* when necessary.
*/
Radar.prototype.checkDistances = function() {
  var distanceWarning = null;

  var myLane = this.myCar.piecePosition.lane.startLaneIndex;
  var carsOnMyLane = this.carsOnLane(myLane);
  for (var i = 0; i < carsOnMyLane.length; i++) {
    var car = carsOnMyLane[i];
    var distance = this.distanceToCar(car);
    if (distance > 0 && distance < this.collisionWarningDistance) {
      //Warn we are about to collide with a car in front of us
      distanceWarning = { type: "collision", direction: "front" };
      break;
    } else if (distance < 0 && distance > -this.collisionWarningDistance) {
      //Warn another car will collide into us from behind
      distanceWarning = { type: "collision", direction: "back" };
      break;
    }
    //console.log("OPPONENT: "+car.id.name+" DISTANCE: "+distance);
  }

  return distanceWarning;
};

/**
* Checks lane blockages. If there's cars on same lane in front that may cause slow downs
*/
Radar.prototype.checkLanes = function() {
  var laneWarning = null;
  var myLane = this.myCar.piecePosition.lane.startLaneIndex;
  var carsOnMyLane = this.carsOnLane(myLane);
  for (var i = 0; i < carsOnMyLane.length; i++) {
    var car = carsOnMyLane[i];
    var distance = this.distanceToCar(car);
    if (distance > 0 && distance < this.laneWarningDistance) {
      laneWarning = { type: "lane", lane: myLane };
    }
  }
  return laneWarning;
};

/**
* Helper method to get all cars on same lane as you.
*/
Radar.prototype.carsOnLane = function(laneIndex) {
  var cars = [];
  for (var i = 0; i < this.otherCars.length; i++) {
    var car = this.otherCars[i];
    var carLane = car.piecePosition.lane.startLaneIndex;
    //Check if car is on desired lane
    if (carLane == laneIndex) {
      cars.push(car);
    }
  }
  return cars;
};

/**
* Returns distance to another car on track.
* Positive value for cars in front of you, negative value for cars behind.
*/
Radar.prototype.distanceToCar = function(otherCar) {
  //TODO: Handle cars near lap change
  var distance = 0; //TODO: change this to something else

  //Calculate my position
  var myLane = this.myCar.piecePosition.lane.startLaneIndex;
  var myPieceIndex = this.myCar.piecePosition.pieceIndex;
  var myFrontPos = this.myCar.frontPosition();
  var myBackPos = this.myCar.backPosition();

  //Calculate other car position
  var otherLane = otherCar.piecePosition.lane.startLaneIndex;
  var otherPieceIndex = otherCar.piecePosition.pieceIndex;
  var otherFrontPos = otherCar.frontPosition();
  var otherBackPos = otherCar.backPosition();

  if (myPieceIndex == otherPieceIndex) {
    //Both cars on same piece

    if (myBackPos > otherFrontPos) {
      //My car is leading other car
      distance = -(myBackPos - otherFrontPos);
    } else if (otherBackPos > myFrontPos) {
      //Other car is leading me
      distance = otherBackPos - myFrontPos;
    } else {
      //At this point we have a collision if we're on the same lane.
    }
  } else if (myPieceIndex > otherPieceIndex) {
    //We are ahead on another piece
      
    distance = myBackPos;
    for (var i = otherPieceIndex; i < myPieceIndex; i++) {
      var trackPiece = this.track.pieces[i];
      if (i == otherPieceIndex) {
	//Current piece of other car, add distance from front of car to end of piece
	distance += (trackPiece.lengths[otherLane]-otherFrontPos);
      } else {
	//Another piece between me and other car, add whole length of piece
	distance += trackPiece.lengths[otherLane];
      }
    }
    //Make negative value
    distance *= -1.0;
  } else if (myPieceIndex < otherPieceIndex) {
    //Other car is ahead on another piece
    //This should never happen unless we are really ahead by a lap xD
    distance = otherBackPos;
    for (var i = myPieceIndex; i < otherPieceIndex; i++) {
      var trackPiece = this.track.pieces[i];
      if (i == myPieceIndex) {
	//Current piece of my car, add distance from front of my car to end of piece
	distance += (trackPiece.lengths[myLane]-myFrontPos);
      } else {
	//Another piece between me and other car, add whole length of piece
	distance += trackPiece.lengths[myLane];
      }
    }
  }
  return distance;
};

module.exports = Radar;
