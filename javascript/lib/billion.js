/**
* @author Team Billion
*/

//Imports
var Track = require("./track.js");
var Car = require("./car.js");
var Throttler = require("./throttler.js")
var Radar = require("./radar.js");

var fs = require('fs');
var log = fs.createWriteStream('speeds4.csv');

//Constants
var laneChangeSector = -1;

//TODO: Move this to OwnCar or similar
var throttler = new Throttler();

var Billion = function(name, index) {
  this.name = name;
  this.index = index;
  this.myCar = null;
  this.track = null;
  this.cars = {};
  this.radar = null;

  this.botProtocol = {
   'carPositions': this.handleCarPositions,
   'turboAvailable': this.handleTurbo
  };

};

Billion.prototype.getResponseMessage = function(data) {
  if (this.botProtocol[data.msgType]) {
    return this.botProtocol[data.msgType].apply(this, [data]);
  } else {
    //TODO: Add these to the protocol
    if (data.msgType === 'join') {
      console.log('Joined');
    } else if (data.msgType == 'yourCar') {
      this.radar = new Radar(data.data);
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType == 'dnf') {
      console.log('dnf');
    } else if (data.msgType == 'gameInit') {
      this.track = new Track(data.data.race.track);
      console.log('Rata: ' + JSON.stringify(this.track.sectors));
      
      for (var i = 0; i < data.data.race.cars.length; i++) {
        this.cars[data.data.race.cars[i].id.name] = new Car(data.data.race.cars[i], this.track);
      }
      
      this.myCar = this.cars[this.name];
      this.radar.trackCars(this.cars, this.track);
    }
    return null;
  }
};

Billion.prototype.handleCarPositions = function(data) {
  //Response contents
  var typeValue;
  var dataValue;
  
  for (var i = 0; i < data.data.length; i++) {
    this.cars[data.data[i].id.name].update(data.data[i]);
  }
  
  var trackUpdate = this.track.update(this.myCar.piecePosition);
  var throttle = throttler.getThrottle(this.myCar, trackUpdate.nextCorner, trackUpdate.targetDistance);
  
  var laneChange = this.track.isLaneChangeNeeded(this.myCar.piecePosition);
 
  //TODO: Move radar warning handling away from this method?
  var radarWarning = this.radar.warnings();
  if (radarWarning != null) {
    //console.log("Received radar warning type: "+radarWarning.type);
    //TODO: Decide how to react to warnings, now slowing down if near collision in front
    if (radarWarning.type == "lane") {
      //TODO: Decide how to react to lane warnings
      var blockedLaneIndex = radarWarning.lane;
      if (blockedLaneIndex == 0) {
	laneChange = "Right";
      } else if (blockedLaneIndex == this.track.lanes.length-1) {
	laneChange = "Left";
      }
    }
    
  }
  if (laneChange && laneChangeSector != this.track.pieces[data.data[0].piecePosition.pieceIndex].sectorIndex) {
    console.log(this.index + ' changing lanes to ' + laneChange);
    laneChangeSector = this.track.pieces[data.data[0].piecePosition.pieceIndex].sectorIndex;
    
    typeValue = "switchLane";
    dataValue = laneChange;
  } else {
    typeValue = "throttle";
    dataValue = throttle;
  }

  //Turbo will override other messages if necessary  
  if (throttler.isTurboAvailable()) {
    if (this.track.shouldCarUseTurbo(this.myCar)) {
      typeValue = "turbo";
      dataValue = "TO THE MOON!";
      throttler.enableTurbo();
    }
  }

  //Each game tick should deplete turbo if it's on
  throttler.depleteTurbo();
  
  var msg = data.gameTick + ';' + this.cars[data.data[0].id.name].speed + ';' + trackUpdate.nextCorner.targetSpeed + ';' + trackUpdate.targetDistance + ';' + throttle + ';' + data.data[0].piecePosition.pieceIndex;
  return { msgType: typeValue, data: dataValue };
};

Billion.prototype.handleTurbo = function(data) {
  throttler.setTurboAvailable(data.data); 
};

module.exports = Billion;
