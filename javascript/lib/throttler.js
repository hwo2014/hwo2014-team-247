var Throttler = function() {
  //TODO: Detect these during warmup
  this.maxSpeed = 10.0;
  this.maxAcc = 0.2;
  this.maxDec = 0.2;
  this.turbo = null;
  
  this.corneringStrategies = {
    'fast': this.fastStrategy,
    'slowInFastOut': this.slowInFastOutStrategy
  };
};

Throttler.prototype.getThrottle = function(car, nextCorner, targetDistance) {
  //TODO: Need to implement proper adjustments to prevent speed going over/under target
  var throttle = 1.0;

  if (targetDistance <= 0) {
    throttle = this.corneringStrategies[nextCorner.strategy].apply(this, [car, nextCorner]);
  } else if (car.speed > nextCorner.targetSpeed) {
    var distanceToReachTargetSpeed = 0;
    var currentSpeed = car.speed;
    while(currentSpeed > nextCorner.targetSpeed) {
      distanceToReachTargetSpeed += currentSpeed;
      var decreasePerTick = (car.speed / this.maxSpeed) * this.maxDec;
      currentSpeed -= decreasePerTick;
    }
    if (distanceToReachTargetSpeed >= targetDistance) {
      throttle = 0.0;
    } else {
      throttle = 1.0;
    }
  } else {
    throttle = 1.0;
  }

  //Handle turbo
  if (this.turbo != null) {
    if (this.turbo.isOn) {
      //TODO: Here we need to decide how turbo affects throttle
      throttle = (throttle/this.turbo.turboFactor);
    }
  }

  return throttle;
};

Throttler.prototype.isTurboAvailable = function() {
  var available = false;
  if (this.turbo != null) {
    if (!this.turbo.isOn && this.turbo.turboDurationTicks > 0) {
      available = true;
    }
  }
  return available;
};

Throttler.prototype.setTurboAvailable = function(turboData) {
  this.turbo = turboData;
  this.turbo.isOn = false;
};

Throttler.prototype.enableTurbo = function() {
  if (this.turbo != null) {
    this.turbo.isOn = true;
  }
};

Throttler.prototype.depleteTurbo = function() {
  if (this.turbo != null) {
    if (this.turbo.isOn) {
      this.turbo.turboDurationTicks -= 1;
      if (this.turbo.turboDurationTicks < 0) {
	this.turbo.turboDurationTicks = 0;
	this.turbo.isOn = false;
      }
    }
  }
};

Throttler.prototype.fastStrategy = function(car, corner) {
    if (corner.targetSpeed < car.speed) {
      return 0.0;
    } else {
      return corner.targetSpeed / 10;
    }
};

Throttler.prototype.slowInFastOutStrategy = function(car, corner) {
  var inCornerDistance = 0;
  for (var i = 0; i < corner.pieces.length; i++) {
    if (corner.pieces[i].pieceIndex == car.piecePosition.pieceIndex) {
      inCornerDistance += car.piecePosition.inPieceDistance;
      break;
    } else {
      inCornerDistance += corner.pieces[i].lengths[car.piecePosition.lane.endLaneIndex];
    }
  }
  
  console.log('slowInFastOut: ' + inCornerDistance + ' ' + corner.laneLengths[car.piecePosition.lane.endLaneIndex]);
  
  if (inCornerDistance*4.5 > corner.laneLengths[car.piecePosition.lane.endLaneIndex]) {
    return Math.min(1.0, inCornerDistance * 3.5 / corner.laneLengths[car.piecePosition.lane.endLaneIndex]);
  } else {
    if (car.speed > corner.targetSpeed) {
      return 0;
    } else {
      return corner.targetSpeed / 10;
    }
  }
};

module.exports = Throttler;
