
var Car = function(data, track) {
  this.id = data.id;
  this.dimensions = data.dimensions;
  this.track = track;
  
  this.angle = 0.0;
  this.piecePosition = {
    pieceIndex: 0,
    inPieceDistance: 0.0,
    lane: {
      startLaneIndex: 0,
      endLaneIndex: 0
    },
    lap: 0
  };
  
  this.speed = 0;
};

Car.prototype.update = function(carPosition) {
  this.calculateSpeed(this.piecePosition, carPosition.piecePosition);
  this.piecePosition = carPosition.piecePosition;
  this.angle = carPosition.angle;
};

Car.prototype.calculateSpeed = function(oldPosition, newPosition) {
  if (oldPosition.pieceIndex == newPosition.pieceIndex) {
    this.speed = newPosition.inPieceDistance - oldPosition.inPieceDistance;
  } else {
    this.speed = this.track.pieces[oldPosition.pieceIndex].lengths[oldPosition.lane.endLaneIndex]
      - oldPosition.inPieceDistance + newPosition.inPieceDistance;
  }
};

/**
* Returns cars front position in the current piece
*/
Car.prototype.frontPosition = function() {
  var inPiecePos = this.piecePosition.inPieceDistance;
  var guideFlagPos = this.dimensions.guideFlagPosition;
  return (inPiecePos+guideFlagPos);
};

/**
* Returns cars back position in current piece
*/
Car.prototype.backPosition = function() {
  var front = this.frontPosition();
  var back = front-this.dimensions.length;
  return back;
};


module.exports = Car;
