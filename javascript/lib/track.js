
var Track = function(data) {
  this.pieces = data.pieces;
  this.lanes = data.lanes;
  
  this.sectors = [[]];
  this.corners = [];
  this.turboPieces = [];
  
  this.parseTrackInfo(this.pieces, this.lanes);
};

Track.prototype.parseTrackInfo = function(pieces, lanes) {
  console.log('parsing data ' + lanes.length);
  TrackUtils.populateSectors(this.sectors, pieces, lanes);
  TrackUtils.populateCorners(this.corners, pieces);
  TrackUtils.populateTurboPieces(this.turboPieces, pieces);
};

Track.prototype.isLaneChangeNeeded = function(piecePosition) {
  var nextSectorIndex = this.pieces[piecePosition.pieceIndex].sectorIndex + 1;
  if (nextSectorIndex == this.sectors.length) {
    nextSectorIndex = 0;
  }
  var nextSector = this.sectors[nextSectorIndex];
  var shortestLane = 0;
  
  for (var i = 0; i < nextSector.length; i++) {
    if (nextSector[i] < nextSector[shortestLane]) {
      shortestLane = i;
    }
  }
  
  if (nextSector[piecePosition.lane.endLaneIndex] == nextSector[shortestLane]) {
    return null;
  }
  
  if (piecePosition.lane.endLaneIndex < shortestLane) {
    return "Right";
  } else if (piecePosition.lane.endLaneIndex > shortestLane) {
    return "Left";
  }
};

Track.prototype.shouldCarUseTurbo = function(car) {
  var isOn = false;
  var carIndex = car.piecePosition.pieceIndex;
  //Currently only using best turbo piece for turbos
  var turboPiece = this.turboPieces[0];
  if (carIndex == turboPiece.index) {
    isOn = true;
  }
  return isOn;
};

Track.prototype.update = function(carPosition) {
  if (this.pieces[carPosition.pieceIndex].corner) {
    return { targetDistance: 0, nextCorner: this.pieces[carPosition.pieceIndex].corner };
  } else {
    var i = carPosition.pieceIndex;
    var corner;
    var distance = this.pieces[carPosition.pieceIndex].lengths[carPosition.lane.endLaneIndex]
                      - carPosition.inPieceDistance;
    
    while (!corner) {
      i = i == this.pieces.length - 1 ? 0 : i + 1;
      corner = this.pieces[i].corner;
      
      if (!corner) {
        distance += this.pieces[i].lengths[carPosition.lane.endLaneIndex];
      }
    }
    
    return { targetDistance: distance, nextCorner: corner };
  }
};

module.exports = Track;

var TrackUtils = {
  populateSectors: function(sectors, pieces, lanes) {
    for (var i = 0; i < pieces.length; i++) {
      pieces[i].pieceIndex = i;
      
      if (pieces[i]['switch']) {
        console.log('switch detected @ ' + i);
        sectors.push([]);
      }
      pieces[i].lengths = [];
      if (!pieces[i]['length']) {
        for (var j = 0; j < lanes.length; j++) {
          var factor = pieces[i].angle < 0 ? 1 : -1;
          var diameter = (pieces[i].radius + (lanes[j].distanceFromCenter * factor)) * 2;
          var laneLength = Math.abs(diameter * Math.PI * pieces[i].angle/360);
          
          pieces[i].lengths.push(laneLength);
          sectors[sectors.length - 1][j] = sectors[sectors.length - 1][j] + laneLength || laneLength;
        }
      } else {
        for (var j = 0; j < lanes.length; j++) {
          pieces[i].lengths.push(pieces[i].length);
          sectors[sectors.length - 1][j] = sectors[sectors.length - 1][j] + pieces[i]['length'] || pieces[i]['length'];
        }
      }
      
      pieces[i].sectorIndex = sectors.length - 1;
    }
  },
  
  populateCorners: function(corners, pieces) {
    for (var i = 0; i < pieces.length; i++) {
      if (pieces[i].radius) {
        var radius = pieces[i].radius;
        var angle = pieces[i].angle;
        for (var j = i + 1; j < pieces.length; j++) {
          if (!pieces[j].radius ||
              TrackUtils.cornerDirection(pieces[j].angle) !== TrackUtils.cornerDirection(angle)) {
            break;
          }
        }
        corners.push(TrackUtils.createCorner(pieces, i, --j));
        i = j;
      }
    }
    
/*    corners[0].strategy = 'slowInFastOut';
    corners[0].targetSpeed = 5.1;
    
    corners[corners.length-1].strategy = 'slowInFastOut';
    corners[corners.length-1].targetSpeed = 5.0;
    
       corners[corners.length-2].targetSpeed = 5.0;*/
  },
  
  cornerDirection: function(angle) {
    return angle < 0 ? 'Left' : 'Right';
  },
  
  createCorner: function(pieces, startPiece, endPiece) {
    var corner = {};
    var cornerPieces = pieces.slice(startPiece, endPiece + 1);
    var laneLengths = [];
    var totalAngle = 0;
  
    for (var i = 0; i < cornerPieces.length; i++) {
      for (var j = 0; j < cornerPieces[i].lengths.length; j++) {
        laneLengths[j] = laneLengths[j] + cornerPieces[i].lengths[j] || cornerPieces[i].lengths[j];
      }
      totalAngle += cornerPieces[i].angle;
      cornerPieces[i].corner = corner;
    }
    
    corner.pieces = cornerPieces;
    corner.laneLengths = laneLengths;
    corner.angle = totalAngle;
    corner.radius = cornerPieces[0].radius;
    
    var fixMe = Math.abs(corner.angle);
    corner.targetSpeed = fixMe > 92 ? 6.5 : fixMe > 80 ? 6.9 : fixMe > 44 ? 10.0 : 10.0;
    corner.strategy = 'fast';
    
    return corner;
  },
  
  calculateDistanceToPiece: function(carPosition, targetPieceIndex) {
    //TODO: maybe not needed...
  },

  //Creates objects of the following format { index: 0, length: 400 }
  //and places them into turboPieces array
  populateTurboPieces: function(turboPieces, pieces) {
    var tempArray = [];
    //Gather potential turbo pieces into array containing arrays of pieces
    for (var i = 0; i < pieces.length; i++) {
      if (!pieces[i].radius) {
	var turboArray = [];
	turboArray.push(pieces[i]);

	for (var j = i+1; j < pieces.length; j++) {
	  if (!pieces[j].radius) {
	    turboArray.push(pieces[j]);
	  } else {
	    break;
	  }
	}
	
	i = j;
	
	//Special case, first piece may contain a potential turbo piece
	//We must add to the end of this piece.
	if (i >= pieces.length) {
	  for (var x = 0; x < pieces.length; x++) {
	    if (!pieces[x].radius) {
	      turboArray.push(pieces[x]);
	    } else {
	      break;
	    }
	  }
	}
	tempArray.push(turboArray);
      }
    }
   
    //Measure length of potential turbo pieces, create turbo piece objects
    //that have index and total length of straight
    for (var x = 0; x < tempArray.length; x++) {
      var turbos = tempArray[x];
      var length = 0;
      for (var y = 0; y < turbos.length; y++) {
	var piece = turbos[y];
	length += piece.length;
      }
      var turboPiece = { index: turbos[0].pieceIndex, length: length };
      turboPieces.push(turboPiece);
    }

    //Sort turbo pieces by length
    turboPieces.sort( function(a,b) {
      return a.length < b.length;
    });
  }

};
